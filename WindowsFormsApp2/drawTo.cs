﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SrijanPanta77227232
{
    class drawTo:Shape
    {
        int x1, y1;

        public drawTo() : base()
        {

        }

        /// <summary>
        /// Drawto Constructor
        /// </summary>
        /// <param name="colour"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="x1"></param>
        /// <param name="y1"></param>
        public drawTo(Color colour, int x, int y, int x1, int y1) : base(colour, x, y)
        {

            this.x1 = x1; //the only thingthat is different from shape
            this.y1 = y1;
        }

        /// <summary>
        /// Overriding set method of Shape class
        /// </summary>
        /// <param name="colour">Colour of line</param>
        /// <param name="list">Parameters for line. </param>

        public override void set(Color colour, params int[] list)
        {
            //list[0] is x, list[1] is y, list[2] is width, list[3] is height
            base.set(colour, list[0], list[1]);
            this.x1 = list[2];
            this.y1 = list[3];
        }

        /// <summary>
        /// Overriding draw method of Shape
        /// </summary>
        /// <param name="g">Graphics Object</param>
        public override void draw(Graphics g)
        {
            Pen p = new Pen(colour, 2);
            Point p1 = new Point(x, y);
            Point p2 = new Point(x1, y1);

            g.DrawLine(p, p1, p2);
        }

        public override double calcArea()
        {
            return 0.0;
        }
        public override double calcPerimeter()
        {
            return 0.0;
        }

    }


}
