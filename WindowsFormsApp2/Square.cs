﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SrijanPanta77227232
{
    /// <summary>
    /// Square class which extends Rectangle.
    /// </summary>
    class Square:Rectangle
    {
        private int size;

        
        public Square() : base()
        {

        }
        /// <summary>
        /// Square constructor
        /// </summary>
        /// <param name="colour"> Colour of Square</param>
        /// <param name="x"> X-cordinate</param>
        /// <param name="y">Y-cordinate</param>
        /// <param name="size">Size of the side</param>
        public Square(Color colour, int x, int y, int size) : base(colour, x, y, size, size)
        {
            this.size = size;
        }

        //no draw method here because it is provided by the parent class Rectangle
        public override void draw(Graphics g)
        {
            base.draw(g);
        }

    }
}
