﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SrijanPanta77227232
{
    /// <summary>
    /// Circle class to draw circle
    /// </summary>
    public class Circle:Shape
    {
        int radius,fill;
        public Circle() : base()
        {

        }
        /// <summary>
        /// Constructor of Circle
        /// </summary>
        /// <param name="colour"> Stores the colour of the circle</param>
        /// <param name="x">X-coordinate</param>
        /// <param name="y">Y-coordinate</param>
        /// <param name="radius"></param>
        public Circle(Color colour, int x, int y, int radius) : base(colour, x, y)
        {

            this.radius = radius; //the only thingthat is different from shape
        }

        /// <summary>
        /// Sets different attributes of circle
        /// </summary>
        /// <param name="colour"> Colour od circle</param>
        /// <param name="list">Parameter list </param>
        public override void set(Color colour, params int[] list)
        {
            //list[0] is x, list[1] is y, list[2] is radius
            base.set(colour, list[0], list[1]);
            this.radius = list[2];
            this.fill = list[4];
        }


        /// <summary>
        /// Overriding draw method of shape to draw a circle as graphics
        /// </summary>
        /// <param name="g"></param>
        public override void draw(Graphics g)
        {

            Pen p;
            if (fill == 1)
            {
                p = new Pen(colour, 0);
                
                SolidBrush b = new SolidBrush(colour);
                g.FillEllipse(b, x, y, radius * 2, radius * 2);
            }
            else
            {
                p = new Pen(colour, 2);
            }
          
            g.DrawEllipse(p, x, y, radius * 2, radius * 2);

        }

        /// <summary>
        /// Method to calculate area of circle
        /// </summary>
        /// <returns> Area of circle</returns>
        public override double calcArea()
        {
            return Math.PI * (radius ^ 2);
        }

        /// <summary>
        /// Method to calculate perimeter of circle
        /// </summary>
        /// <returns>Perimeter of circle</returns>
        public override double calcPerimeter()
        {
            return 2 * Math.PI * radius;
        }

        /// <summary>
        /// Method to return radius of circle
        /// </summary>
        /// <returns>base class attributes and radius of circle</returns>
        public override string ToString() //all classes inherit from object and ToString() is abstract in object
        {
            return base.ToString() + "  " + this.radius;
        }
    }
}
