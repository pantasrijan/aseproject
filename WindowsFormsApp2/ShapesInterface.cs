﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SrijanPanta77227232
{
    /// <summary>
    /// Shape Interface
    /// </summary>
    interface Shapes
    {
        /// <summary>
        /// method to set different parameters of a shape.
        /// </summary>
        /// <param name="c">Colour</param>
        /// <param name="list">List of parameteres</param>
        void set(Color c, params int[] list);
        /// <summary>
        /// Method to draw shapes.
        /// </summary>
        /// <param name="g">Graphics object</param>
        void draw(Graphics g);
        /// <summary>
        /// Method to calculate area
        /// </summary>
        /// <returns>Area</returns>
        double calcArea();
        /// <summary>
        /// Method to calculate perimeter
        /// </summary>
        /// <returns>Perimeter</returns>
        double calcPerimeter();
    }
}
