﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SrijanPanta77227232
{
    public partial class MainForm : Form
    {
        //Variable declarations
        Thread newThread;
        bool flag = false, running = true;
        bool ifFlag = true, whileFlag = true, methodFlag = true;
        DataTable dataTable = new DataTable();
        public List<string> parameterName = new List<string>();
        public List<string> methodVariables = new List<string>();
        public List<int> methodVariablesValues = new List<int>();
        public List<int> parameterValue = new List<int>();
        public int methodStartLine;
        ShapeFactory factory = new ShapeFactory();
        int errorLine = 0;
        string methodName, methodParameters;
        Color colour1=System.Drawing.Color.White, colour2= System.Drawing.Color.White;
        int methodCount;
        ArrayList shapes = new ArrayList();
        Shape s;
        int x, y, Integer;
        int ifCount = 0, whileCount = 0, endIfCount = 0, endWhileCount = 0;
        System.ArgumentException argEx;
       
        
        public MainForm()
        {
            InitializeComponent();
            newThread = new System.Threading.Thread(thread);
            newThread.IsBackground = true;
            newThread.Start();


        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string about = "Assignment Details: \n\n" +
                            "Advanced Software Engeneering Component 1 & 2 \n" +
                            "Shape Factory used to draw different type of shapes \n" +
                            "Consist of picturbox, textbox and buttons \n" +
                            "Error handeling is done aswell. \n" +
                            "Multi thread to flash colours on the screen. \n\n" +
                            "-------------------------------------------------------------\n" +
                            "Syntax Formats:\n" +
                            "Rectangle 10,20 \n" +
                            "Circle 30 \n" +
                            "Triangle 20,30 \n" +
                            "Square 20\n" +
                            "Pen red \n" +
                            "fill on\n" +
                            "a = 10\n" +
                            "While a < 10\n" +
                            "endwhile \n" +
                            "if a < 10 \n" +
                            "endif\n" +
                            "Method mymethod (a,b,c)\n" +
                            "Lines of code \n" +
                            "endmethod \n" +
                            "mymethod(10,20,30)\n" +
                            "-------------------------------------------------------------\n\n" +
                            "Submitted by: \n" +
                            "Srijan Panta \n" +
                            "77227232";
            MessageBox.Show(about,"About");
        }

        /// <summary>
        /// Method for multithreading
        /// Changes colour of pictureBox
        /// </summary>
        public void thread()
        {
            while (true)
            {
                while (running == true)
                {
                    if (flag == false)
                    {

                        this.pictureBoxOutput.BackColor = colour1;
                        flag = true;
                    }
                    else
                    {
                        this.pictureBoxOutput.BackColor = colour2;
                        flag = false;
                    }
                    Thread.Sleep(500);
                }
            }
        }

        /// <summary>
        /// Calling drawing function from click of execute button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExecute_Click(object sender, EventArgs e)
        {
            drawShapes();

        }

        /// <summary>
        /// Calling draw function from Enter Key press.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textSingleInput_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                drawShapes();
            }
        }

       
        /// <summary>
        /// Function to draw shape in the pictureBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void paintOutput(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            
            for (int i = 0; i < shapes.Count; i++)
            {
                Shape s;
                s = (Shape)shapes[i];
                if (s != null)
                {
                    s.draw(g);
                }
                else
                    Console.WriteLine("invalid shape in array"); //shouldn't happen as factory does not produce rubbish
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFile = new SaveFileDialog();

            saveFile.InitialDirectory = @"C:\";
            saveFile.RestoreDirectory = true;
            saveFile.FileName = "*.txt";
            saveFile.DefaultExt = "txt";
            saveFile.Filter = "txt files (*.txt) |*.txt";

            if(saveFile.ShowDialog() == DialogResult.OK)
            {
                Stream fileStream = saveFile.OpenFile();
                StreamWriter streamWriter = new StreamWriter(fileStream);

                streamWriter.Write(txtInput.Text);
                
                streamWriter.Close();
                fileStream.Close();

            }
        }

        
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();

            openFile.InitialDirectory = @"C:\";
            openFile.RestoreDirectory = true;

            openFile.FileName = "*.txt";
            openFile.DefaultExt = "txt";
            openFile.Filter = "txt files (*.txt) |*.txt";

            if (openFile.ShowDialog() == DialogResult.OK)
            {
                Stream fileStream = openFile.OpenFile();
                StreamReader streamReader = new StreamReader(fileStream);

                txtInput.Text = streamReader.ReadToEnd();

                streamReader.Close();
                fileStream.Close();

            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to quit the program? \n All unsaved changes will be lost.", "Exit", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {

                Application.Exit();

            }
        }

            
        
        /// <summary>
        /// Function where all the text from input box are splitted
        /// Creation of objects of shapes
        /// Storing shapes in an array
        /// Refreshing the pictureBox
        /// </summary>
        private void drawShapes()
        {
           
           
            pictureBoxOutput.Image = null;
            shapes.Clear();
            methodVariables.Clear();
            parameterName.Clear();
            parameterValue.Clear();
            txtError.Text = "";
            methodCount = 0;
            ifCount = 0;
            whileCount = 0;
            endIfCount = 0;
            endWhileCount = 0;

            if (textSingleInput.Text.ToUpper() == "RUN")
            {
               
                /*
                 * try block to deal with exceptions
                 */
                try
                {
                    /*
                     * Only executing if the input isn't empty
                     */
                    if (txtInput.Text != "")
                    {

                        //Splitting lines of text from TextBox
                        String[] input = new String[] { "\r\n" };
                        String[] lines = txtInput.Text.Split(input, StringSplitOptions.RemoveEmptyEntries);
                        errorLine = 0;
                        commands(lines);

                        
                    }
                    //refresh the picturebox so the old images will be deleted and new image will be loaded. 
                    pictureBoxOutput.Refresh();
                }

                /*
                 * Catch blocks to catch all the exception and show error message in textBox
                 */
                catch (ArgumentException ergEx)
                {
                     pictureBoxOutput.Image = null;
                    shapes.Clear();
                    txtError.Text = "Line " + errorLine + ":- " + ergEx.Message;
                }
                catch (System.FormatException)
                {
                    pictureBoxOutput.Image = null;
                    shapes.Clear();
                    txtError.Text = "Line " + errorLine + ":- Co-ordinates and sizes must be integers.";
                }
                catch (System.Data.SyntaxErrorException)
                {
                    pictureBoxOutput.Image = null;
                    shapes.Clear();
                    txtError.Text = "Line " + errorLine + ":- Syntax Error.";
                }
            }
            else if (textSingleInput.Text.ToUpper() == "CLEAR")
            {
                pictureBoxOutput.Image = null;
                pictureBoxOutput.BackColor = Color.White;
                running = false;
                shapes.Clear();
            }
            else if (textSingleInput.Text.ToUpper() == "RESET")
            {
                x = 0;
                y = 0;
            }
            else
            {
                txtError.Text = "Invalid single line command to execute";
            }

        }

        /// <summary>
        /// Method to process every command submitted line by line
        /// </summary>
        /// <param name="lines">Array of lines from textbox</param>
        public void commands(String[] lines)
        {
            //initializing all the different attributes of the shape
            x = 0;
            y = 0;
            int size = 0;
            int size1 = 0;
            int fill = 0;
            int whileCounter = 0;
            Color newColour = Color.Black;
            
           



            /*
             * Loop to split every single line of textInput with appropriate delimiters
             * Creating Object of shapes and storing them in an array
             */
            for (int index = 0; index < lines.Length; index++)
            {
                errorLine=index+1;

                String[] splitLine = lines[index].Split(' ');
                if (splitLine.Length > 0)
                {

                    /*
                        * if clause to handle "moveto" command entered by user
                        * throws error if entered incorrectly.
                        */
                    if (splitLine[0].ToUpper() == "MOVETO" && splitLine[1].Contains(',') && splitLine.Length == 2)
                    {
                        if(whileFlag && ifFlag && methodFlag)
                        {
                            String[] splitSize = splitLine[1].Split(',');
                            if (splitSize.Length == 2)
                            {
                                if (int.TryParse(splitSize[0], out Integer) == false)
                                {
                                    int paraIndex = parameterName.IndexOf(splitSize[0].ToUpper());
                                    if (paraIndex > -1)
                                    {
                                        x = parameterValue[paraIndex];

                                    }
                                    else
                                    {
                                        throw new System.ArgumentException(splitSize[0] + " has not been declared");
                                    }

                                }
                                else
                                {
                                    x = Int16.Parse(splitSize[0]);
                                }
                                if (int.TryParse(splitSize[1], out Integer) == false)
                                {
                                    int paraIndex = parameterName.IndexOf(splitSize[1].ToUpper());
                                    if (paraIndex > -1)
                                    {
                                        y = parameterValue[paraIndex];

                                    }
                                    else
                                    {
                                        throw new System.ArgumentException(splitSize[1] + " has not been declared");
                                    }

                                }
                                else
                                {
                                    y = Int16.Parse(splitSize[1]);
                                }



                            }
                            else
                            {
                                argEx = new System.ArgumentException("Input not in the format moveto <int>,<int>");
                                throw argEx;
                            }
                        }
                       

                    }
                    else if (splitLine[0].ToUpper() == "COLOUR" && splitLine.Length == 2)
                    {
                        if (splitLine[1].ToUpper() == "REDGREEN")
                        {
                            colour1 = System.Drawing.Color.Red;
                            colour2 = System.Drawing.Color.Green;
                            running = true;

                        }
                        else if (splitLine[1].ToUpper() == "BLUEYELLOW")
                        {
                            colour1 = System.Drawing.Color.Blue;
                            colour2 = System.Drawing.Color.Yellow;
                            running = true;

                        }
                        else if (splitLine[1].ToUpper() == "BLACKWHITE")
                        {
                            colour1 = System.Drawing.Color.Black;
                            colour2 = System.Drawing.Color.White;
                            running = true;

                        }
                        else
                        {
                            argEx = new System.ArgumentException("Not available colour combination");
                            throw argEx;
                        }
                    }
                    else if (splitLine[0].ToUpper()== "METHOD" && splitLine.Length == 3)
                    {
                        if(splitLine[2].Contains("(") && splitLine[2].Contains(")"))
                        {
                            methodName = splitLine[1].ToUpper();
                            methodParameters = splitLine[2].Replace("(","");
                            methodParameters = methodParameters.Replace(")","");
                            String[] splitParameters = methodParameters.Split(',');
                            for (int i = 0; i<splitParameters.Count(); i++)
                            {
                                methodVariables.Add(splitParameters[i]);
                            }
                            methodStartLine = index;
                            methodFlag = false;
                            
                        }
                        else
                        {
                            throw new System.ArgumentException("Invalid method call.");
                        }
                    }
                    else if (splitLine[0].ToUpper() == methodName && splitLine.Length == 2)
                    {
                        if (splitLine[1].Contains("(") && splitLine[1].Contains(")"))
                        {
                            string methodValues = splitLine[1].Replace("(", "");
                            methodValues = methodValues.Replace(")", "");
                            string[] splitValues = methodValues.Split(',');
                            if (methodVariables.Count() == splitValues.Length)
                            {
                                for(int i = 0; i<methodVariables.Count();i++)
                                {
                                    if(int.TryParse(splitValues[i], out Integer))
                                    {
                                        parameterName.Add(methodVariables[i].ToUpper());
                                        parameterValue.Add(Int16.Parse(splitValues[i]));
                                    }
                                    else
                                    {
                                        throw new System.ArgumentException("Invalid values for paramters.");
                                    }
                                    
                                }

                                if(methodCount==0)
                                {
                                    index = methodStartLine;
                                    methodCount++;
                                }
                                

                            }
                            else
                            {
                                throw new System.ArgumentException("Invalid number of parameters.");

                            }
                        }

                    }
                    /*
                        * else if clause to handle "rectangle" command entered by user
                        * throws error if entered incorrectly
                        */
                    else if (splitLine[0].ToUpper() == "RECTANGLE" && splitLine[1].Contains(',') && splitLine.Length == 2)
                    {
                        if (whileFlag && ifFlag && methodFlag)
                        {
                            String[] splitSize = splitLine[1].Split(',');
                            if (splitSize.Length == 2)
                            {
                                if (int.TryParse(splitSize[0], out Integer) == false)
                                {
                                    int paraIndex = parameterName.IndexOf(splitSize[0].ToUpper());
                                    if (paraIndex > -1)
                                    {
                                        size = parameterValue[paraIndex];

                                    }
                                    else
                                    {
                                        throw new System.ArgumentException(splitSize[0] + " has not been declared");
                                    }

                                }
                                else
                                {
                                    size = Int16.Parse(splitSize[0]);
                                }
                                if (int.TryParse(splitSize[1], out Integer) == false)
                                {
                                    int paraIndex = parameterName.IndexOf(splitSize[1].ToUpper());
                                    if (paraIndex > -1)
                                    {
                                        size1 = parameterValue[paraIndex];

                                    }
                                    else
                                    {
                                        throw new System.ArgumentException(splitSize[1] + " has not been declared");
                                    }

                                }
                                else
                                {
                                    size1 = Int16.Parse(splitSize[1]);
                                }
                                s = factory.getShape(splitLine[0]);
                                s.set(newColour, x, y, size, size1, fill);
                                shapes.Add(s);

                            }
                            else
                            {
                                argEx = new System.ArgumentException("Input not in the format rectangle <int>,<int>");
                                throw argEx;
                            }
                        }
                        else
                        {
                            continue;
                        }

                    }
                    /*
                        * else if clause to handle "drawto" command entered by user
                        * throws error if entered incorrectly
                        */
                    else if (splitLine[0].ToUpper() == "DRAWTO" && splitLine[1].Contains(',') && splitLine.Length == 2)
                    {
                        if(whileFlag && ifFlag && methodFlag)
                        {
                            String[] splitSize = splitLine[1].Split(',');
                            if (splitSize.Length == 2)
                            {
                                if (int.TryParse(splitSize[0], out Integer) == false)
                                {
                                    int paraIndex = parameterName.IndexOf(splitSize[0].ToUpper());
                                    if (paraIndex > -1)
                                    {
                                        size = parameterValue[paraIndex];

                                    }
                                    else
                                    {
                                        throw new System.ArgumentException(splitSize[0] + " has not been declared");
                                    }

                                }
                                else
                                {
                                    size = Int16.Parse(splitSize[0]);
                                }
                                if (int.TryParse(splitSize[1], out Integer) == false)
                                {
                                    int paraIndex = parameterName.IndexOf(splitSize[1].ToUpper());
                                    if (paraIndex > -1)
                                    {
                                        size1 = parameterValue[paraIndex];

                                    }
                                    else
                                    {
                                        throw new System.ArgumentException(splitSize[1] + " has not been declared");
                                    }

                                }
                                else
                                {
                                    size1 = Int16.Parse(splitSize[1]);
                                }

                                s = factory.getShape(splitLine[0]);
                                s.set(newColour, x, y, size, size1);
                                shapes.Add(s);

                            }
                            else
                            {
                                argEx = new System.ArgumentException("Input not in the format drawto <int>,<int>");
                                throw argEx;
                            }
                            
                        }
                        else
                        {
                            continue;
                        }

                    }
                    /*
                        * else if clause to handle "circle" and "square" command entered by user
                        * throws error if entered incorrectly
                        */
                    else if (splitLine[0].ToUpper() == "CIRCLE" || splitLine[0].ToUpper() == "SQUARE" && splitLine.Length == 2)
                    {
                        if(whileFlag && ifFlag && methodFlag)
                        {
                            if(splitLine[1].Contains(','))
                            {
                                throw new System.ArgumentException("One one value required.");
                            }
                            if (int.TryParse(splitLine[1], out Integer) == false)
                            {
                                int paraIndex = parameterName.IndexOf(splitLine[1].ToUpper());
                                if (paraIndex > -1)
                                {
                                    size = parameterValue[paraIndex];

                                }
                                else
                                {
                                    throw new System.ArgumentException(splitLine[1] + " has not been declared");
                                }

                            }
                            else
                            {
                                size = Int16.Parse(splitLine[1]);
                            }
                            s = factory.getShape(splitLine[0]);
                            s.set(newColour, x, y, size, size, fill);
                            shapes.Add(s);
                        }
                        else
                        {
                            continue;
                        }
                        
                    }
                    /*
                        * else if clause to handle "triangle" command entered by user
                        * throws error if entered incorrectly
                        */
                    else if (splitLine[0].ToUpper() == "TRIANGLE" && splitLine[1].Contains(',') && splitLine.Length == 2)
                    {
                        if(whileFlag && ifFlag && methodFlag)
                        {
                            String[] splitSize = splitLine[1].Split(',');
                            if (splitSize.Length == 2)
                            {

                                if (int.TryParse(splitSize[0], out Integer) == false)
                                {
                                    int paraIndex = parameterName.IndexOf(splitSize[0].ToUpper());
                                    if (paraIndex > -1)
                                    {
                                        size = parameterValue[paraIndex];

                                    }
                                    else
                                    {
                                        throw new System.ArgumentException(splitSize[0] + " has not been declared");
                                    }

                                }
                                else
                                {
                                    size = Int16.Parse(splitSize[0]);
                                }
                                if (int.TryParse(splitSize[1], out Integer) == false)
                                {
                                    int paraIndex = parameterName.IndexOf(splitSize[1].ToUpper());
                                    if (paraIndex > -1)
                                    {
                                        size1 = parameterValue[paraIndex];

                                    }
                                    else
                                    {
                                        throw new System.ArgumentException(splitSize[1] + " has not been declared");
                                    }

                                }
                                else
                                {
                                    size1 = Int16.Parse(splitSize[1]);
                                }

                                s = factory.getShape(splitLine[0]);
                                s.set(newColour, x, y, size, size1, fill);
                                shapes.Add(s);
                            }

                            else
                            {
                                argEx = new System.ArgumentException("Input not in the format triangle <int>,<int>");
                                throw argEx;
                            }

                        }
                        else
                        {
                            continue;
                        }
                        
                    }
                    /*
                        * else if clause to handle "pen" command entered by user
                        * throws error if entered incorrectly or if colour isn't defined
                        */
                    else if (splitLine[0].ToUpper() == "PEN" && splitLine.Length == 2 && !splitLine[1].Contains(',') && splitLine.Length == 2)
                    {
                        if(whileFlag && ifFlag && methodFlag)
                        {
                            if (splitLine[1].ToUpper() == "RED")
                            {
                                newColour = Color.Red;
                                continue;
                            }
                            else if (splitLine[1].ToUpper() == "ORANGE")
                            {
                                newColour = Color.Orange;
                                continue;
                            }
                            else if (splitLine[1].ToUpper() == "YELLOW")
                            {
                                newColour = Color.Yellow;
                                continue;
                            }
                            else if (splitLine[1].ToUpper() == "GREEN")
                            {
                                newColour = Color.Green;
                                continue;
                            }
                            else if (splitLine[1].ToUpper() == "BLUE")
                            {
                                newColour = Color.Blue;
                                continue;
                            }
                            else if (splitLine[1].ToUpper() == "INDIGO")
                            {
                                newColour = Color.Indigo;
                                continue;
                            }
                            else if (splitLine[1].ToUpper() == "VIOLET")
                            {
                                newColour = Color.Violet;
                                continue;
                            }

                            else
                            {
                                argEx = new System.ArgumentException("Color not available-> " + splitLine[1]);
                                throw argEx;
                            }
                        }
                        else
                        {
                            continue;
                        }
                        
                    }
                        /*
                        * else if clause to handle "fill" command entered by user
                        * throws error if entered incorrectly
                        */
                    else if (splitLine[0].ToUpper() == "FILL" && splitLine.Length == 2 && !splitLine[1].Contains(',') && splitLine.Length == 2)
                    {
                        if(whileFlag && ifFlag && methodFlag)
                        {
                            if (splitLine[1].ToUpper() == "ON")
                            {
                                fill = 1;
                                continue;
                            }
                            else if (splitLine[1].ToUpper() == "OFF")
                            {
                                fill = 0;
                                continue;
                            }
                            else
                            {
                                argEx = new System.ArgumentException("Only on/off available for fill");
                                throw argEx;
                            }
                        }
                        else
                        {
                            continue;
                        }
                      
                    }

                    /*
                     * Handeling declaration of variables eg. a = 40
                     */
                    else if (splitLine.Length == 3)
                    {
                        if (whileFlag && ifFlag && methodFlag)
                        {
                            if (splitLine[1] == "=")
                            {

                                if (int.TryParse(splitLine[2], out Integer) == true && int.TryParse(splitLine[0], out Integer) == false)
                                {

                                    parameterName.Add(splitLine[0].ToUpper());
                                    parameterValue.Add(Int16.Parse(splitLine[2]));
                                }
                                else if (int.TryParse(splitLine[0], out Integer) == true)
                                {
                                    throw new System.ArgumentException("Parameter name cannot be integer");
                                }
                            }
                        }
                        else
                        {
                            continue;
                        }
                        

                    }
                    /*
                     * Checking for while statement
                     */
                    else if (splitLine.Length == 4)
                    {
                        if (splitLine[0].ToUpper() == "WHILE")
                        {
                            whileCount++;
                            if (int.TryParse(splitLine[1], out Integer) == false && int.TryParse(splitLine[2], out Integer) == false && int.TryParse(splitLine[3], out Integer) == true)
                            {
                                int paraIndex = parameterName.IndexOf(splitLine[1].ToUpper());
                                if (paraIndex > -1)
                                {
                                    string value = parameterValue[paraIndex].ToString();
                                    bool operationResult = statementChecker(value, splitLine );
                                    if(operationResult==false)
                                    {
                                        whileFlag = false;
                                        continue;
                                    }
                                    else
                                    {
                                        whileCounter = index;
                                    }
                                }
                                else
                                {
                                    throw new System.ArgumentException("Variable is not declared.");
                                }

                            }
                            else
                            {
                                throw new System.ArgumentException("Syntax incorect for while");
                            }
                        }
                        /*
                         * Checking for If clause in user input
                         */
                        else if (splitLine[0].ToUpper() == "IF")
                        {
                            ifCount++;
                            if (int.TryParse(splitLine[1], out Integer) == false && int.TryParse(splitLine[2], out Integer) == false && int.TryParse(splitLine[3], out Integer) == true)
                            {
                                int paraIndex = parameterName.IndexOf(splitLine[1].ToUpper());
                                if (paraIndex > -1)
                                {
                                    string value = parameterValue[paraIndex].ToString();
                                    bool operationResult = statementChecker(value, splitLine); //checks for condition of statement
                                    if (operationResult == false)
                                    {
                                        ifFlag = false;
                                        continue;
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }
                                else
                                {
                                    throw new System.ArgumentException("Variable is not declared");
                                }

                            }
                            
                            else
                            {
                                throw new System.ArgumentException("Syntax incorect for if");
                            }
                        }

                    }
                    else if (splitLine.Length == 1)
                    {
                        //Checking for endwhile
                        
                        if (splitLine[0].ToUpper() == "ENDWHILE")
                        {
                            if(whileFlag==false)
                            {
                                whileFlag = true;
                                endWhileCount++;
                                continue;
                            }
                            else
                            {
                                index = whileCounter-1;
                                continue;
                                
                            } 
                            
                        }
                        else if (splitLine[0].ToUpper() == "ENDIF")
                        {
                            endIfCount++;
                            ifFlag = true;
                        }
                        else if (splitLine[0].ToUpper() == "ENDMETHOD")
                        {
                            methodFlag = true;
                        }
                    }
                    /*
                     * Incrementing value of vairable 
                     * Throws error if variable doesnt exist
                     */
                    else if (splitLine.Length == 5)
                    {
                        if (splitLine[1] == "=" && splitLine[3] == "+" || splitLine[3] == "%")
                        {
                            if (whileFlag && ifFlag && methodFlag)
                            {
                                if (int.TryParse(splitLine[4], out Integer) == true && int.TryParse(splitLine[0], out Integer) == false && splitLine[0] == splitLine[2])
                                {
                                    int paraIndex = parameterName.IndexOf(splitLine[0].ToUpper());

                                    if (paraIndex > -1)
                                    {
                                        if(splitLine[3]=="+")
                                        {
                                            parameterValue[paraIndex] = parameterValue[paraIndex] + Int16.Parse(splitLine[4]);
                                        }
                                        else
                                        {
                                            if(parameterValue[paraIndex]>0)
                                            {
                                                parameterValue[paraIndex] = parameterValue[paraIndex] % Int16.Parse(splitLine[4]);
                                            }
                                            else
                                            {
                                                parameterValue[paraIndex] = 0;
                                            }
                                           
                                        }
                                    }
                                    else
                                    {
                                        throw new System.ArgumentException("Variable is not declared");
                                    }
                                }
                                else
                                {
                                    throw new System.ArgumentException("Syntax error");
                                }
                            }
                            else
                            {
                                continue;
                            }

                        }
                    }
                    //Throwing syntax error if nothing matches our conditions
                    else
                    {
                        argEx = new System.ArgumentException("Syntax Error-> "+lines[index]);
                        throw argEx;
                    }

                }

                /*
                 * else clause to throw error
                 */
                else
                {
                    argEx = new System.ArgumentException("Incorrect input format");
                    throw argEx;
                }
            }
        }

        /// <summary>
        /// Method to check the condition of while and IF statements
        /// </summary>
        /// <param name="value">Value of the vairable passed</param>
        /// <param name="splitLine">Other attributes of statement in array</param>
        /// <returns></returns>
        public bool statementChecker(string value, String[] splitLine)
        {
          
            string operation = value + " " + splitLine[2] + " " + splitLine[3];
            bool operationResult = (bool)dataTable.Compute(operation, null);
            return operationResult;
        }
    }
}
