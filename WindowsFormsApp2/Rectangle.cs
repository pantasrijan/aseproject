﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SrijanPanta77227232
{
    class Rectangle:Shape
    {

        int width, height, fill;
        public Rectangle() : base()
        {
            width = 100;
            height = 100;

            
        }
        /// <summary>
        /// Rectangle Constructor
        /// </summary>
        /// <param name="colour">Colour of rectangle.</param>
        /// <param name="x">X-cordinate</param>
        /// <param name="y">Y-cordinate</param>
        /// <param name="width">Width of rectangle</param>
        /// <param name="height">Height of rectangle</param>
        public Rectangle(Color colour, int x, int y, int width, int height) : base(colour, x, y)
        {

            this.width = width; //the only thingthat is different from shape
            this.height = height;
        }
        /// <summary>
        /// Overriding set method of Shape class
        /// </summary>
        /// <param name="colour">Colour of rectangle</param>
        /// <param name="list">Parameters for rectangle. </param>
       
        public override void set(Color colour, params int[] list)
        {
            //list[0] is x, list[1] is y, list[2] is width, list[3] is height
            base.set(colour, list[0], list[1]);
            this.width = list[2];
            this.height = list[3];
            this.fill = list[4];
        }

        /// <summary>
        /// Overriding draw method of Shape by rectangle.
        /// </summary>
        /// <param name="g">Instance of graphics.</param>
        public override void draw(Graphics g)
        {
            Pen p;
            if (fill==1)
            {
                p = new Pen(colour, 0);
                SolidBrush b = new SolidBrush(colour);
                g.FillRectangle(b, x, y, width, height);
            }
            else
            {
                p = new Pen(colour, 2);
            }
            g.DrawRectangle(p, x, y, width, height);
        }

        /// <summary>
        /// Overriding calcArea of Shape by Rectangle
        /// </summary>
        /// <returns>Area of Rectangle</returns>
        public override double calcArea()
        {
            return width * height;
        }

        /// <summary>
        /// Overriding calcPerimeter of Shape by Rectangle
        /// </summary>
        /// <returns>Perimeter of Rectangle</returns>
        public override double calcPerimeter()
        {
            return 2 * width + 2 * height;
        }
    }
}
