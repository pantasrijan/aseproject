﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SrijanPanta77227232
{
    public class Triangle:Shape
    {
        
        int width, height, fill;

        
        public Triangle() : base()
        {
            width = 100;
            height = 100;
        }
        
        /// <summary>
        /// Triangle Constructor
        /// </summary>
        /// <param name="colour">Colour of Triangle</param>
        /// <param name="x">X-Cordinate</param>
        /// <param name="y">Y-Cordinate</param>
        /// <param name="width">Width/Base of triangle.</param>
        /// <param name="height">Height of triangle</param>
        public Triangle(Color colour, int x, int y, int width, int height) : base(colour, x, y)
        {
            this.width = width;
            this.height = height;
        }
        /// <summary>
        /// Set method to set all the attributes of triangle
        /// It overrides the set method of Shape Class
        /// </summary>
        /// <param name="colour"> Colour of triangle</param>
        /// <param name="list">Attributes Lists</param>
        public override void set(Color colour, params int[] list)
        {
            //list[0] is x, list[1] is y
            //list[0] is x, list[1] is y, list[2] is width, list[3] is height
            base.set(colour, list[0], list[1]);
            this.width = list[2];
            this.height = list[3];
            this.fill = list[4];
        }

        /// <summary>
        /// Draw method of triangle which override draw method of shape
        /// </summary>
        /// <param name="g"> Instance of graphics.</param>
        public override void draw(Graphics g)
        {
            Point[] points = { new Point(x,y), new Point(x+width, y+height), new Point(x, y+height) };
            
            Pen p;
            if (fill == 1)
            {
                p = new Pen(colour, 0);
                SolidBrush b = new SolidBrush(colour);
                g.FillPolygon(b, points);
            }
            else
            {
                p = new Pen(colour, 2);
            }
            
            g.DrawPolygon(p,points);
        }
        /// <summary>
        /// Overriding calArea of Shape
        /// </summary>
        /// <returns>Area of Triangle</returns>
        public override double calcArea()
        {
            double area = 1 / 2 * (width * height);
            return area;
        }
        /// <summary>
        /// Overriding calPerimeter of Shape
        /// </summary>
        /// <returns>Perimeter of Triangle</returns>
        public override double calcPerimeter()
        {
            double perimeter;
            float hypotenuse = ((width ^ 2) + (height ^ 2)) ^ (1 / 2);
            perimeter = width + height + hypotenuse;
            return perimeter;
        }
    }

    
}
