﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Drawing;
using SrijanPanta77227232;
using System.Collections.Generic;

namespace ShapesUnitTest
{
    [TestClass]
    public class UnitTest1
    {
        /// <summary>
        /// Unit test for testing the calcArea of Rectangle Class.
        /// </summary>
        [TestMethod]
        public void CalculateAreaOfRectangleTest()
        {
            // Arrange
            var factory = new ShapeFactory();
            var shapes = "rectangle";
            var expected = 2500 ;
            int width = 50, height = 50;
           


            //Act
            var actual=factory.getShape(shapes);
            actual.set(Color.Red,0,0,width, height,1);
 
            //Assert
            Assert.AreEqual(expected,actual.calcArea());

        }
        /// <summary>
        /// Test method to calculate the perimeter of rectangle
        /// </summary>
        [TestMethod]
        public void CalculatePerimeterOfRectangleTest()
        {
            // Arrange
            var factory = new ShapeFactory();
            var shapes = "rectangle";
            double expected = 400;
            int height = 100, width=100;



            //Act

            var actual = factory.getShape(shapes);
            actual.set(Color.Red,0,0,height,width,1);


            //Assert
            Assert.AreEqual(expected,actual.calcPerimeter());

        }

        /// <summary>
        /// Test method to check for the condition of if statement
        /// </summary>
        [TestMethod]
        public void StatementCheckerTestForIf()
        {
            // Arrange
            var test = new MainForm();
            var command = "if a < 20";
            var splitLine = command.Split(' ');
            int valueOfa = 10 ;
            var expected = true;

            //Act
            var actual = test.statementChecker(valueOfa.ToString(), splitLine);


            //Assert
            Assert.AreEqual(expected, actual);

        }

        /// <summary>
        /// Test method to check for the condition of while statement
        /// </summary>
        [TestMethod]
        public void StatementCheckerTestForWhileInvalidValue()
        {
            // Arrange
            var test = new MainForm();
            var command = "while a < 20";
            var splitLine = command.Split(' ');
            int valueOfa = 30;
            var expected = false;

            //Act
            var actual = test.statementChecker(valueOfa.ToString(), splitLine);


            //Assert
            Assert.AreEqual(expected, actual);

        }

        /// <summary>
        /// Test method to check if the value of vairable will be stored in the array or not.
        /// </summary>
        [TestMethod]
        public void VariableTest()
        {
            // Arrange
            var test = new MainForm();
            String[] command = { "a = 10" };
            test.commands(command);
            var expected = 10;


            //Act
            int index = test.parameterName.IndexOf("a".ToUpper());
            var actual = test.parameterValue[index];


            //Assert
            Assert.AreEqual(expected, actual);

        }
    }
}
